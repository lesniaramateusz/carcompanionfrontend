import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';



@Injectable({
  providedIn: 'root'
})

export class ProfileService {

  private user_request:Observable<User>

  getUserProfile(){

    if(!this.user_request){
     this.user_request = this.http
     .get<User>('')
     .pipe(
        shareReplay()
      )
    }
    return this.user_request
  }

  clearCashe(){
    this.user_request = null
  }

  constructor(private http:HttpClient, private auth:AuthService){

   }
}

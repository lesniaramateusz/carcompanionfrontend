import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { ProfileService } from './profile.service';
import { User } from './user';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profile:User;
   user;





  constructor(private profileServices:ProfileService, protected auth: AuthService) {

  }

  ngOnInit(): void {
    const profile$ = this.profileServices.getUserProfile()
    profile$.subscribe(user => {
     this.profile = user
    })
    this.auth.email.subscribe( user =>{
      this.user = user
    })
  }

}

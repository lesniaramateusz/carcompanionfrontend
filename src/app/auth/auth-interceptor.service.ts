import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService  implements HttpInterceptor {


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {



    //req.headers.append('Authorization','Bearer ' + this.auth.getaceToken())
/*
   req.clone({
    setHeaders:{
      'Authorization': 'Bearer ' + this.auth.getaceToken()
    }
   })

 *//*
     return next.handle(this.getAuthorizedreq(req))


  }
    getAuthorizedreq(req: HttpRequest<any>){
     return req.clone({
    setHeaders: {
       'Authorization': 'Bearer ' + this.auth.getaceToken()

      }
    })
/**/
    const token: string = this.auth.getaceToken();


    if (token) {
      req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) });
    }

    if (!req.headers.has('Content-Type')) {
      req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    }

    req = req.clone({ headers: req.headers.set('Accept', 'application/json') });

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {

          console.log('event--->>>', event);
        }
        return event;
      }));



    }

 constructor(private auth:AuthService) { }

}


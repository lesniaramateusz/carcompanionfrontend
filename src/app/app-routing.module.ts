import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { AuthorizedGuard } from './auth/authorized.guard';
import { ProfileEditComponent } from './profile/profile-edit/profile-edit.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditCarComponent } from './dashboard/edit-car/edit-car.component';

const route: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [
      AuthorizedGuard
    ]
  },
  {
    path: 'registration',
    component: RegistrationComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'profileEdit',
    component: ProfileEditComponent,
    canActivate: [
      AuthorizedGuard
    ]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [
      AuthorizedGuard
    ]
  },
  {
    path: 'carEdit',
    component: EditCarComponent,
    canActivate: [
      AuthorizedGuard
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(route,{
      enableTracing: true,
      //initialNavigation:true,
      useHash: true
    })

  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
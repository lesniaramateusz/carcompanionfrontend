import { Component, OnInit, ɵSWITCH_TEMPLATE_REF_FACTORY__POST_R3__ } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DashboardService } from './dashboard.service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {


  cars: Array<any> = [];

  shareKey = this.fb.group({
    key:['']
  })

  postCar = this.fb.group({
    brand: [''],
    model: [''],
    generation: [''],
    plate: [''],
    mileage: [],
    productionYear: []
  });


  constructor(private fb: FormBuilder, private dash: DashboardService, public router: Router) {}

  createTable(){
    this.postCar = this.fb.group({
      brand: [''],
      model: [''],
      generation: [''],
      plate: [''],
      mileage: [],
      productionYear: []
    });
  }
//

  consolelog(cars) {
    console.log(cars);
  }

  addCar() {
    this.dash.postCar(this.postCar.value);
    setTimeout(()=>{this.ngOnInit()},1000);

  }

  getCarById(editcar:any){
    this.dash.getCarById(editcar);
    console.log(editcar)
  }

  shareCar(){
    console.log('dashboard Component to ma '+this.shareKey.value.key)
    this.dash.subscribeCare(this.shareKey.value.key);
    setTimeout(()=>{this.ngOnInit()},1000);
  }

  ngOnInit(): void {

   this.dash.getCars();
    this.dash.allcars.subscribe(response => {
    this.cars = response;
      this.consolelog(this.cars);
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DashboardService } from '../dashboard.service';
interface Cars {
  carId: string;
  mainName: string;
  brand: string;
  model: string;
  generation: string;
  plate: string;
  mileage: number;
  productionYear: number;
}

@Component({
  selector: 'app-edit-car',
  templateUrl: './edit-car.component.html',
  styleUrls: ['./edit-car.component.css']
})
export class EditCarComponent implements OnInit {
  editCar = this.fb.group({
    mainName: [''],
    brand: [''],
    model: [''],
    generation: [''],
    plate: [''],
    mileage: [],
    productionYear: []
  });


  Role: any = ['editor', 'viewer']

  shareField = this.fb.group({
    role:['']
  })

  postExpense = this.fb.group({
    amount:[],
    description:[''],
    mileageInterval:[],
    date:[''],
    endOfDateInterval:[''],
    category:[''],
  });


 key;
 allexpanses;
 category;
 car;
 edited = false;
 deleted = false;
  constructor(private fb:FormBuilder, private dash:DashboardService) {
   }

   shareCar(){
     this.dash.shareCar(this.shareField.value,this.car.carId);
   }

  updateCar(){
    this.dash.updateCar(this.editCar.value,this.car.carId);
    this.edited = true;
  }
  deleteCar(){
    this.dash.deleteCar(this.car.carId);
      this.deleted = true;
  }
  getExpense(){
    this.dash.getExpense(this.car.carId);
  }
  addExpense(){
    this.dash.addExpense(this.postExpense.value,this.car.carId);
  }

  ngOnInit(): void {
   // this.getExpense();
 //   this.dash.getCategory();
 //   this.dash.allcategory.subscribe(response =>{
  //    this.category = response;
  //    console.log(response);
  //  });
    this.dash.shareKey.subscribe(response=>{
      this.key = response
    })
    this.dash.carid.subscribe((response:Cars) => {
      this.car = response;
     console.log('edit component to ma',this.car)
    });
  }
}
